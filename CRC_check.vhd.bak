library ieee;
use ieee.std_logic_1164.all;

entity CRC_check is
   port(
   -- Inputs
      clk            :     in std_logic;
      nrst           :     in std_logic;
      CRC_check_EN   :     in std_logic;
      CRC_data       :     in std_logic_vector(39 downto 0);

   -- Outputs      
      CRC_finish     :     out std_logic;
      CRC_result     :     out std_logic;
      CRC_check_busy :     out std_logic
   );
end CRC_check;

architecture rtl of CRC_check is
      type     STATE_TYPE is (IDLE, CRC_GEN);
      signal   STATE         :   state_type; 
      signal   counter       :   integer range -1 to 31;
      signal   r_data        :   std_logic_vector(31 downto 0);
      signal   r_crc         :   std_logic_vector(8 downto 0);
      constant poly          :   std_logic_vector(8 downto 0) := "100101111";
begin

   process(clk, nrst) begin
      if nrst = '0' then
         STATE <= IDLE;
         CRC_finish <= '0';
         CRC_result <= '0';
         CRC_check_busy <= '0';
      elsif clk'event and clk = '1' then

         case (STATE) is

            when IDLE =>
               CRC_finish <= '0';
               CRC_finish <= '0';
               counter <= 31;
               if(CRC_check_EN = '1') then
                  CRC_check_busy <= '1';
                  STATE <= CRC_GEN;
                  r_data <= CRC_data(30 downto 0) & '0'; 
                  r_crc <= CRC_data(39 downto 31);
               end if;

            when CRC_GEN =>
               counter <= counter - 1;
               if(r_crc(8) = '1') then
                  r_crc <= (r_crc(7 downto 0) xor poly(7 downto 0)) & r_data(31);
               else
                  r_crc <= (r_crc(7 downto 0) xor "00000000") & r_data(31);
               end if;

               r_data <= r_data(30 downto 0) & '0';
               if(counter = 0) then
                  if r_crc(7 downto 0) = "00000000" then
                     CRC_result <= '0';
                  else
                     CRC_result <= '1';
                  end if;
                  CRC_finish <= '1';
                  CRC_check_busy <= '0';
                  STATE <= IDLE;
               end if;
         end case;

     end if;
   end process;

end rtl;

