library ieee;
use ieee.std_logic_1164.all;

entity CRC_gen is
   port(
   -- Inputs
      clk            :     in std_logic;
      nrst           :     in std_logic;
      CRC_gen_EN     :     in std_logic;
      Data           :     in std_logic_vector(31 downto 0);

   -- Outputs      
      CRC_value      :     out std_logic_vector(7 downto 0);
      CRC_RDY        :     out std_logic;
      CRC_gen_busy   :     out std_logic
   );
end CRC_gen;

architecture rtl of CRC_gen is
      type     STATE_TYPE    is (IDLE, CRC_GEN);
      signal   STATE         :   STATE_TYPE; 
      signal   counter       :   integer range -1 to 32;
      signal   r_data        :   std_logic_vector(30 downto 0);
      signal   r_crc         :   std_logic_vector(8 downto 0);
      -- constant poly          :   std_logic_vector(5 downto 0) := "101100";
      constant poly          :   std_logic_vector(8 downto 0) := "100101111";
begin

   process(clk, nrst) begin
      if nrst = '0' then
         STATE <= IDLE;
         CRC_value <= "00000000";
         CRC_RDY <= '0';
         CRC_gen_busy <= '0';
      elsif clk'event and clk = '1' then

         case(STATE) is

            when IDLE =>
               counter <= 32;
               CRC_RDY <= '0';
               if(CRC_gen_EN = '1') then
                  STATE <= CRC_GEN;
                  r_data <= Data(22 downto 0) & "00000000"; 
                  r_crc <= Data(31 downto 23);
                  CRC_gen_busy <= '1';
               end if;

            when CRC_GEN =>
               counter <= counter - 1;
               if(r_crc(8) = '1') then
                  r_crc <= (r_crc(7 downto 0) xor poly(7 downto 0)) & r_data(30);
               else
                  r_crc <= (r_crc(7 downto 0) xor "00000000") & r_data(30);
               end if;

               r_data <= r_data(29 downto 0) & '0';
               if(counter = 0) then
                  CRC_value <= r_crc(8 downto 1);
                  CRC_RDY <= '1';
                  CRC_gen_busy <= '0';
                  STATE <= IDLE;
               end if;
         end case;

     end if;
   end process;

end rtl;

