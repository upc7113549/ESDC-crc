library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity LCD_disp is
  port(
    -- Signals to/from the rest of the system
    clk        :  in std_logic;
    nrst       :  in std_logic;
    stat_err   :  in std_logic_vector(16 downto 0);
    tx_counter :  in std_logic_vector(3 downto 0);
    disp_stat  :  in std_logic;
    disp_prog  :  in std_logic;
    mode       :  in std_logic;
    mode_valid :  in std_logic;

    -- Signals to/from LCD 
    DB_LCD     :  inout std_logic_vector (7 downto 0);
    ON_LCD     :  out std_logic;
    E_LCD      :  out std_logic;
    RW_LCD     :  out std_logic;
    RS_LCD     :  out std_logic
  );
end LCD_disp;

architecture rtl of LCD_disp is
  type STATE_TYPE     is (S_IDLE,
                          S_WAIT_BUSY,
                          S_INIT_DISP1,
                          S_INIT_DISP2,
                          S_CLR,
                          S_RHOME,
                          S_INIT_PROG1,
                          S_INIT_PROG2,
                          S_INIT_PROG3,
                          S_INIT_PROG4,
                          S_PROG1, -- print arrow
                          S_PROG2, -- shift
                          S_PROG3, -- return home
                          S_STAT1, -- print receive message
                          S_STAT2, -- print number of errors
                          S_STOP); -- do nothing

  signal STATE          : STATE_TYPE;
  signal RETURN_STATE   : STATE_TYPE;

  signal   tx_counter_r : std_logic_vector(3 downto 0);
  signal   clk_en       : std_logic;
  signal   clk_counter  : integer;
  signal   busy_counter : integer;
  constant busy_divider : integer := 256;
  constant clk_divider  : integer := 16;
  signal   char_address : integer range 0 to 18;
  signal   char         : character;
  signal   num_counter  : integer range 0 to 4;
  signal   num_address  : std_logic_vector(3 downto 0);
  signal   num          : character;
  signal   reg_stat_err : std_logic_vector(15 downto 0);
  signal   E_LCD_INT    : std_logic;
  signal   E_LCD_INT_D  : std_logic;
  signal   q_mode_valid : std_logic;
begin

  process (clk, nrst) begin
    if (nrst = '0') then
      clk_counter <= 0;
      clk_en      <= '0';
    elsif clk'event and clk = '1' then
      if (clk_counter = clk_divider - 1) then
        clk_counter <= 0;
        clk_en      <= '1';
      else
        clk_counter <= clk_counter + 1;
        clk_en      <= '0';
      end if;
    end if;
  end process;

  process (clk, nrst) begin
    if (nrst = '0') then
      STATE        <= S_INIT_DISP1;
      RETURN_STATE <= S_INIT_DISP1;
      tx_counter_r <= x"0";
      busy_counter <= 0;
      RW_LCD       <= '0';
      RS_LCD       <= '0';
      E_LCD_INT    <= '0';
      E_LCD_INT_D  <= '0';
      char_address <= 0;
      num_address  <= "0000";
      num_counter  <= 0;
      reg_stat_err <= x"0000";
      ON_LCD       <= '1';
      q_mode_valid <= '0';
    elsif clk'event and clk = '1' then
      E_LCD_INT_D <= E_LCD_INT;
      E_LCD <= E_LCD_INT_D;
      if (clk_en = '1') then
        q_mode_valid <= mode_valid;
        case (STATE) is
          when S_WAIT_BUSY =>
            DB_LCD <= "ZZZZZZZZ";
            RW_LCD <= '1';
            RS_LCD <= '0';

            if (busy_counter = busy_divider - 1) then
              busy_counter <= 0;
            else
              busy_counter <= busy_counter + 1;
            end if;

            if (busy_counter = busy_divider - 1) then
              E_LCD_INT <= '1';
            else 
              E_LCD_INT <= '0';
            end if;

            if (E_LCD_INT = '1') then
              if (DB_LCD(7) = '0') then
                STATE <= RETURN_STATE;
              end if;
            end if;

          when S_INIT_DISP1 =>
            DB_LCD <= "001111XX";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_INIT_DISP2;
            end if;

          when S_INIT_DISP2 =>
            DB_LCD <= "00001100";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_CLR;
            end if;

          when S_CLR =>
            DB_LCD <= "00000001";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_RHOME;
            end if;

          when S_RHOME =>
            DB_LCD <= "0000001X";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_IDLE;
            end if;


          when S_IDLE =>
            if (mode = '1' and mode_valid = '1' and q_mode_valid = '0') then
              STATE <= S_INIT_PROG1;
            elsif (disp_stat = '1') then
              STATE <= S_STAT1;
            elsif (disp_prog = '1') then
              tx_counter_r <= "0000";
              STATE <= S_PROG1;
            end if;
    
          when S_INIT_PROG1 => -- print [
            DB_LCD <= conv_std_logic_vector(character'pos('['),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_INIT_PROG2;
            end if;

          when S_INIT_PROG2 => -- go to address 13
            DB_LCD <= "10001101";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_INIT_PROG3;
            end if;

          when S_INIT_PROG3 => --print ]
            DB_LCD <= conv_std_logic_vector(character'pos(']'),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_INIT_PROG4;
            end if;

          when S_INIT_PROG4 => -- go to address 1
            DB_LCD <= "10000001";
            RW_LCD <= '0';
            RS_LCD <= '0';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_IDLE;
            end if;


          when S_PROG1 => -- print arrow
            DB_LCD <= conv_std_logic_vector(character'pos('>'),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              RETURN_STATE <= S_PROG2;
            end if;

          when S_PROG2 => -- shift cursor to the left when needed
            if (tx_counter /= tx_counter_r) then
              DB_LCD <= "000100XX";
              RW_LCD <= '0';
              RS_LCD <= '0';
              if (E_LCD_INT = '0') then
                E_LCD_INT <= '1';
              else
                E_LCD_INT        <= '0';
                tx_counter_r <= tx_counter;
                STATE        <= S_WAIT_BUSY;
                RETURN_STATE <= S_PROG3;
              end if;
            end if;

          when S_PROG3 => -- print euqal
            DB_LCD <= conv_std_logic_vector(character'pos('='),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              if (tx_counter_r = "1100") then
                RETURN_STATE <= S_STOP;
              else
                RETURN_STATE <= S_PROG1;
              end if;
            end if;

          when S_STAT1 =>
            DB_LCD <= conv_std_logic_vector(character'pos(char),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              if (char_address = 9) then
                RETURN_STATE <= S_STAT2;
                num_address  <= "000" & stat_err(16);
                reg_stat_err <= stat_err(15 downto 0);
              else
                char_address <= char_address + 1;
                RETURN_STATE <= S_STAT1;
              end if;
            end if;

          when S_STAT2 =>
            DB_LCD <= conv_std_logic_vector(character'pos(num),8);
            RW_LCD <= '0';
            RS_LCD <= '1';
            if (E_LCD_INT = '0') then
              E_LCD_INT <= '1';
            else
              E_LCD_INT        <= '0';
              STATE        <= S_WAIT_BUSY;
              if (num_counter = 4) then
                RETURN_STATE <= S_STOP;
              else
                num_address  <= reg_stat_err(15 downto 12);
                num_counter  <= num_counter + 1;
                reg_stat_err <= reg_stat_err(11 downto 0) & "0000";
                RETURN_STATE <= S_STAT2;
              end if;
            end if;



          when S_STOP =>
            STATE <= S_STOP;

        end case;
      end if;
    end if;
  end process;


  with char_address select
    char <= 'E' when 0,
            'r' when 1,
            'r' when 2,
            'o' when 3,
            'r' when 4,
            's' when 5,
            ':' when 6,
            ' ' when 7,
            '0' when 8,
            'x' when 9,
            ' ' when others;

  with num_address select
    num <= '0' when x"0",
           '1' when x"1",
           '2' when x"2",
           '3' when x"3",
           '4' when x"4",
           '5' when x"5",
           '6' when x"6",
           '7' when x"7",
           '8' when x"8",
           '9' when x"9",
           'A' when x"A",
           'B' when x"B",
           'C' when x"C",
           'D' when x"D",
           'E' when x"E",
           'F' when x"F",
           ' ' when others;

end rtl;
