# Constrain clock port clk with a 20-ns requirement (50 MHz)
create_clock -name crc_clk -period 20 [get_ports OSC_50]
# Constrain clock for VGA, obtained in the design by a clock divider
# (comment next line if no VGA is used)
# create_generated_clock  -name clk_vga -source [get_ports OSC_50] -divide_by 2 [get_registers I_O_Manager_VGA:inst27|VGA_visualisation:inst1|clk_div_two:inst9|clk_int]

# Automatically apply a generate clock on the output of phase-locked loops (PLLs)
# This command can be safely left in the SDC even if no PLLs exist in the design

derive_pll_clocks

# Constrain the input I/O path (all are asynchronous)
# arbitrary input delays with respect to my_clk
#set_input_delay -clock crc_clk 3 [get_ports COL*]
#set_input_delay -clock crc_clk 3 [get_ports UART_RXD]
#set_input_delay -clock crc_clk 3 [get_ports KEY[0]]
#set_input_delay -clock crc_clk 3 [get_ports DPDT_SW[0]]
#set_input_delay -clock crc_clk 3 [get_ports DPDT_SW[1]]
#set_input_delay -clock crc_clk 10 [get_ports LCD_DATA[7]]

#False paths for asynch inputs
set_false_path -from [get_ports COL*]
set_false_path -from [get_ports UART_RXD]
set_false_path -from [get_ports KEY[0]]
set_false_path -from [get_ports DPDT_SW[0]]
set_false_path -from [get_ports DPDT_SW[1]]
set_false_path -from [get_ports LCD_DATA[7]]

# Constrain the output I/O path (all are asynchronous)
# arbitrary input delays with respect to my_clk
#set_output_delay -clock crc_clk 200 [all_outputs]

#False paths for asynch outputs
set_false_path -to [all_outputs]