library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity main is
   port(
   -- Inputs
      clk                :     in std_logic;
      nrst               :     in std_logic;
      DATA_Rx            :     in std_logic_vector(7 downto 0);
      Data               :     in std_logic_vector(31 downto 0);
      Data_valid         :     in std_logic;
      CRC_value          :     in std_logic_vector(7 downto 0);
      CRC_ready          :     in std_logic;
      RX_ready           :     in std_logic;
      TX_ready           :     in std_logic;
      Err_inj_cnt        :     in std_logic_vector(15 downto 0);
      prng_valid         :     in std_logic;
      CRC_gen_busy       :     in std_logic;
      CRC_check_busy     :     in std_logic;
      CRC_finish         :     in std_logic;
      mode               :     in std_logic;
      mode_valid         :     in std_logic;
      CRC_check_result   :     in std_logic;

   -- Outputs
      CRC_data_check     :     out std_logic_vector(39 downto 0);
      CRC_data_gen       :     out std_logic_vector(31 downto 0);
      frame_read         :     out std_logic;
      stat_err           :     out std_logic_vector(16 downto 0);
      CRC_gen_EN         :     out std_logic;
      CRC_check_EN       :     out std_logic;
      TX_Data            :     out std_logic_vector(7 downto 0);
      tx_send            :     out std_logic;
      led_status         :     out std_logic;
      led_ack_error      :     out std_logic;
      disp_stat          :     out std_logic;
      disp_prog          :     out std_logic;
      stop_prng          :     out std_logic;
      data_to_display    :     out std_logic_vector(31 downto 0);
      tx_counter_lcd     :     out std_logic_vector(3 downto 0)
   );
end main;

architecture rtl of main is
  type     STATE_TYPE     is (IDLE,
                              T_IDLE,
                              T_GEN_CRC,
                              T_TRANSMIT_FRAME,
                              T_WAIT_ACK,
                              T_CHECK_ACK,
                              T_END,
                              R_WAIT_FRAME,
                              R_CHECK_CRC,
                              R_DISPLAY_DATA,
                              R_SEND_ACK,
                              R_DISPLAY_STAT);

  signal   STATE              :   STATE_TYPE;
  signal   status             :   std_logic_vector(2 downto 0); -- 1 - Receiver, 0 - Transmitter
  signal   r_data             :   std_logic_vector(31 downto 0);
  signal   crc_done           :   boolean;
  signal   ack_done           :   boolean;
  constant timeout_limit      :   integer := 750000000; -- 15 sec timeout
  signal   timeout_counter    :   unsigned(29 downto 0);
  signal   t_enabled          :   std_logic;
  signal   r_enabled          :   std_logic;
  signal   tx_counter         :   unsigned(16 downto 0);
  signal   rx_counter         :   unsigned(16 downto 0);
  signal   tx_byte_counter    :   integer range 0 to 5;
  signal   rx_byte_counter    :   integer range 0 to 5;
  signal   tx_data_reg        :   std_logic_vector(39 downto 0);
  signal   frame_read_int     :   std_logic;
  constant frame_number       :   integer := 100000;  -- Number of frames to send
  constant inj_mask           :   std_logic_vector(39 downto 0) := x"0000000000"; -- Mask to be applied for error injection
  signal   CRC_gen_EN_int     :   std_logic;
  signal   CRC_data_gen_int   :   std_logic_vector(31 downto 0);
  signal   CRC_data_check_int :   std_logic_vector(39 downto 0);
  signal   stat_err_int       :   std_logic_vector(16 downto 0);
  signal   tx_send_int        :   std_logic;
begin

  process (clk, nrst) begin
    if nrst = '0' then
      STATE              <= IDLE;
      CRC_gen_EN_int     <= '0';
      tx_send_int        <= '0';
      tx_counter         <= '0'  & x"0000";
      timeout_counter    <= "00" & x"0000000";
      CRC_check_EN       <= '0';
      tx_byte_counter    <= 0;
      rx_byte_counter    <= 0;
      CRC_data_check_int <= x"0000000000";
      rx_counter         <= x"0000" & '0';
      tx_data_reg        <= x"0000000000";
      stop_prng          <= '0';
      frame_read_int     <= '0';
      CRC_data_gen_int   <= x"00000000";
      stat_err_int       <= x"0000" & '0';
      disp_prog          <= '0';
      disp_stat          <= '0';
      led_status         <= '0';
      led_ack_error      <= '0';
      data_to_display    <= x"00000000";
    elsif (clk'event and clk = '1') then
  
      case(STATE) is
        when IDLE =>
          -- TODO: Check for how long mode_valid is up
          if (mode_valid = '1') then
            stop_prng  <= '1';
            if (mode = '0') then
              STATE <= R_WAIT_FRAME;
            else 
              led_status <= '1';
              STATE      <= T_IDLE;
            end if;
          end if;

        when T_IDLE =>
          if (Data_valid = '1' and prng_valid = '1') then
            disp_prog <= '1';
            STATE     <= T_GEN_CRC;
          end if;

        when T_GEN_CRC =>
          tx_send_int <= '0';
          if (CRC_gen_busy = '0' and CRC_ready = '0') then
            CRC_gen_EN_int   <= '1'; 
            CRC_data_gen_int <= std_logic_vector((unsigned(Data) + tx_counter));
          elsif (CRC_gen_EN_int = '1') then
            CRC_gen_EN_int <= '0'; 
          end if;

          if (CRC_ready = '1') then
            STATE       <= T_TRANSMIT_FRAME;
            if (tx_counter < unsigned(Err_inj_cnt)) then
              tx_data_reg <= (CRC_data_gen_int & CRC_value) xor inj_mask;
            else
              tx_data_reg <= (CRC_data_gen_int & CRC_value);
            end if;
          end if;

        when T_TRANSMIT_FRAME =>
          if (tx_byte_counter = 5 and TX_ready = '1') then
            tx_counter   <= tx_counter + 1;
            tx_byte_counter <= 0;
          end if;

          if (tx_send_int = '0' and TX_ready = '1' and tx_byte_counter /= 5) then
            tx_send_int     <= '1';
            TX_Data         <= tx_data_reg(39 downto 32);
            tx_data_reg     <= tx_data_reg(31 downto 0) & x"00";
            tx_byte_counter <= tx_byte_counter + 1;
          else
            tx_send_int     <= '0';
          end if;

          if (tx_counter = frame_number) then
            STATE <= T_WAIT_ACK;
          elsif (tx_byte_counter = 5) then
            STATE <= T_GEN_CRC;
          end if;

        when T_WAIT_ACK =>
          tx_counter      <= '0'  & x"0000";
          timeout_counter <= timeout_counter + 1;
          if (timeout_counter = timeout_limit) then
            STATE <= T_GEN_CRC;
          elsif (rx_counter = 5) then
            STATE      <= T_CHECK_ACK;
          end if;

          if (frame_read_int = '1') then
            frame_read_int  <= '0';
            rx_byte_counter <= rx_byte_counter + 1;
            CRC_data_check_int <= CRC_data_check_int(31 downto 0) & DATA_Rx;
          elsif (RX_ready = '1') then
            frame_read_int <= '1';
          end if;

        when T_CHECK_ACK =>
          if (CRC_check_busy = '0' and CRC_finish = '0') then
            CRC_check_EN <= '1';
          elsif (CRC_finish = '1') then
            led_ack_error <= CRC_check_result;
            CRC_check_EN  <= '0';
          end if;

          if (CRC_finish = '1') then
            STATE <= T_END;
          end if;

        when T_END =>
          STATE       <= T_END;

        when R_WAIT_FRAME =>
          if (frame_read_int = '1') then
            frame_read_int  <= '0';
            rx_byte_counter <= rx_byte_counter + 1;
            CRC_data_check_int <= CRC_data_check_int(31 downto 0) & DATA_Rx;
          elsif (RX_ready = '1') then
            frame_read_int <= '1';
          end if;

          if (rx_byte_counter = 5) then
            STATE <= R_CHECK_CRC;
          end if;

        when R_CHECK_CRC =>
          rx_byte_counter <= 0;
          if (CRC_check_busy = '0' and CRC_finish = '0') then
            CRC_check_EN <= '1';
          else
            CRC_check_EN <= '0';
          end if;

          if (CRC_finish = '1') then
            if (CRC_check_result = '1') then
              stat_err_int <= std_logic_vector(unsigned(stat_err_int) + 1);
            end if;
            STATE <= R_DISPLAY_DATA;
          end if;

        when R_DISPLAY_DATA =>
          data_to_display <= CRC_data_check_int(39 downto 8);
          rx_counter <= rx_counter + 1;
          if (rx_counter = frame_number - 1) then
            STATE <= R_SEND_ACK;
          else
            STATE <= R_WAIT_FRAME;
          end if;

        when R_SEND_ACK =>
          TX_Data           <= x"00";
          if (tx_byte_counter = 5 and TX_ready = '1') then
            tx_counter      <= tx_counter + 1;
            tx_byte_counter <= 0;
            STATE <= R_DISPLAY_STAT;
          end if;

          if (tx_send_int = '0'and TX_ready = '1' and tx_byte_counter /= 5) then
            tx_send_int <= '1';
            tx_byte_counter <= tx_byte_counter + 1;
          else
            tx_send_int <= '0';
          end if;

        when R_DISPLAY_STAT =>
          disp_stat <= '1';
          STATE     <= R_DISPLAY_STAT;
      end case;
    end if;
  end process;


  frame_read     <= frame_read_int;
  CRC_gen_EN     <= CRC_gen_EN_int;
  CRC_data_gen   <= CRC_data_gen_int;
  CRC_data_check <= CRC_data_check_int;
  stat_err       <= stat_err_int;
  tx_send        <= tx_send_int;
  tx_counter_lcd <= std_logic_vector(tx_counter(16 downto 13));

end rtl;
