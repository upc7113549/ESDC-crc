library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity prng is
  port (
    -- Inputs
    clk         :   in  std_logic;
    nrst        :   in  std_logic;
    stop        :   in  std_logic;

    -- Outputs
    rand_n      :   out std_logic_vector(15 downto 0);
    rand_valid  :   out std_logic
  );
end prng;

architecture rtl of prng is
  signal count     : unsigned(15 downto 0);
  signal stop_det  : std_logic;
begin
  process(clk, nrst) begin
    if nrst = '0' then
      count      <=  x"0000";
      rand_valid <= '0';
      stop_det   <= '0';
      rand_n     <=  x"0000";
    elsif clk'event and clk = '1' then
      if (stop_det = '0') then
        count <= count + 1;
      else
        rand_n     <= std_logic_vector(count);
        rand_valid <= '1';
      end if;

      if (stop = '1') then
        stop_det <= '1';
      end if;
    end if;
  end process;


end rtl;
