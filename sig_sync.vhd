library ieee;
use ieee.std_logic_1164.all;

entity sig_sync is
port( 
-- Inputs
    clk     : in  std_logic;
    nrst    : in  std_logic;
    sig_in  : in  std_logic;
    sig_out : out std_logic
);
end sig_sync;

architecture rtl of sig_sync is
  signal q_sig : std_logic;
begin

  process(clk, nrst) begin
    if nrst = '0' then
      q_sig   <= '0';
      sig_out <= '0';
    elsif clk'event and clk = '1' then
      q_sig   <= sig_in;
      sig_out <= q_sig;
    end if;
  end process;

end rtl;
