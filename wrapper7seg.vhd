library work;
library ieee;
use ieee.std_logic_1164.all;

entity wrapper7seg is
  port (
    data_in     : in std_logic_vector(31 downto 0);
    hex0        : out std_logic_vector(6 downto 0);
    hex1        : out std_logic_vector(6 downto 0);
    hex2        : out std_logic_vector(6 downto 0);
    hex3        : out std_logic_vector(6 downto 0);
    hex4        : out std_logic_vector(6 downto 0);
    hex5        : out std_logic_vector(6 downto 0);
    hex6        : out std_logic_vector(6 downto 0);
    hex7        : out std_logic_vector(6 downto 0)
  );
end wrapper7seg;

architecture rtl of wrapper7seg is
  component hex7seg
    port ( num  : in std_logic_vector (3 downto 0);
           HEX  : out std_logic_vector (6 downto 0));
  end component;
begin

  display0: hex7seg
    port map(data_in(3 downto 0), hex0);

  display1: hex7seg
    port map(data_in(7 downto 4), hex1);

  display2: hex7seg
    port map(data_in(11 downto 8), hex2);

  display3: hex7seg
    port map(data_in(15 downto 12), hex3);

  display4: hex7seg
    port map(data_in(19 downto 16), hex4);

  display5: hex7seg
    port map(data_in(23 downto 20), hex5);

  display6: hex7seg
    port map(data_in(27 downto 24), hex6);

  display7: hex7seg
    port map(data_in(31 downto 28), hex7);

end rtl;
