library work;
library ieee;
use ieee.std_logic_1164.all;

entity wrapper_keypad is
  port (
    -- Inputs
    clk        : in  std_logic;
    nrst       : in  std_logic;
    col        : in  std_logic_vector(3 downto 0);

    -- Outputs
    data       : out std_logic_vector(31 downto 0);
    data_valid : out std_logic;
    row        : out std_logic_vector(3 downto 0)
  );
end wrapper_keypad;

architecture rtl of wrapper_keypad is
  type   STATE_TYPE is (IDLE,
                        READ_KEY,
                        CONFIRM_DATA,
                        STOP);

  signal STATE       : STATE_TYPE;
  signal key_valid   : std_logic;
  signal key_code    : std_logic_vector(3 downto 0);

  signal data_int    : std_logic_vector(31 downto 0);
  signal key_counter : integer range 0 to 8;

  component keypad
    port (clk     : in  std_logic;
          nrst    : in  std_logic;
          col     : in  std_logic_vector(3 downto 0);
          key     : out std_logic;
          keycode : out std_logic_vector(3 downto 0);
          row     : out std_logic_vector(3 downto 0));
  end component;
begin

  process (clk, nrst) begin
    if nrst = '0'then
      STATE       <= IDLE;
      data_int    <= x"00000000";
      data_valid  <= '0';
      key_counter <= 0;
    elsif clk = '1' and clk'event then
      case (STATE) is
        when IDLE =>
          if (key_code = x"e" and key_valid = '1') then -- E is the same as * on the keypad
            STATE <= READ_KEY;
          end if;

        when READ_KEY =>
          if (key_valid = '1') then
            key_counter <= key_counter + 1;
            data_int    <= data_int(27 downto 0) & key_code;
          end if;

          if (key_counter = 8) then
            STATE <= CONFIRM_DATA;
          end if;

        when CONFIRM_DATA =>
          if (key_valid = '1') then
            if (key_code = x"f") then -- F is the same as # on the keypad
              data_valid <= '1';
              STATE      <= STOP;
            elsif (key_code = x"e") then -- E is the same as * on the keypad
              STATE       <= READ_KEY;
              key_counter <= 0;
            end if;
          end if;

        when STOP =>
          STATE <= STOP;
      end case;
    end if;
  end process;



  u_keypad: keypad
    port map(
      clk     => clk,
      nrst    => nrst,
      col     => col,
      key     => key_valid,
      keycode => key_code,
      row     => row
    );

  data <= data_int;
end rtl;
